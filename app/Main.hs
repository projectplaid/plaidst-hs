module Main where

import           System.Environment
import           Data.List
import           Plaidst.Scanner

main :: IO ()
main = do
  args <- getArgs
  case args of
    []      -> putStrLn "Start an interpreter"
    [fname] -> do
      putStrLn ("Compile " ++ fname)
      source <- readFile fname
      let tokens = Plaidst.Scanner.scanTokens source
      mapM_ print tokens
    _       -> putStrLn "Too many arguments"
