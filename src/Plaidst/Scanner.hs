module Plaidst.Scanner where

import           Data.Char (isAlphaNum, isDigit, isSpace)
import           Integer.Natural

data TokenType =
    StIdentifier String
  | StInteger Int
  | StFloat Float
  | StSymbol String
  | StComment String
  | StString String
  | StNil
  | StTrue
  | StFalse
  | StThisContext
  | StSelf
  | EOF
  deriving (Show, Eq)

data Token =
  Token { tokenType :: TokenType, line :: Natural, column :: Natural }
  deriving (Show, Eq)

scanTokens :: String -> [Token]
scanTokens [] = []
